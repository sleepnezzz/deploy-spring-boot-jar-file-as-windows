# ขั้นตอนการ Deploy a Spring Boot Application บน Windows Service มี 2 วิธี ดังนี้

### 1. ขั้นตอนการรัน Service โดยใช้ไฟล์ RUN.bat
- 1.1 สร้างไฟล์ RUN.bat
- 1.2 แก้ไขไฟล์ RUN.bat โดยเพิ่มคำสั่ง java -jar <ชื่อไฟล์ตามด้วย .jar> จากนั้นก็กด save โลด
- 1.3 Double Click ไฟล์ RUN.bat เป็นอัน จอบอ

>ปล. ขั้นตอนนี้จะเป็นการรัน jar file ผ่าน CMD ปกติ และจะแสดงหน้า Windows Terminal ไว้
                
----
### 2. ขั้นตอนการรัน Service โดยให้ระบบ startup บน local services ของ Windows
- 2.1 นำไฟล์  WinSW.NET4.exe มาไว้ใน folder ที่มี jar file อยุ่ (สามารถ download ได้ที่ https://github.com/kohsuke/winsw/releases) ตอนนี้ให้อ้างอิง version winsw-v2.1.1 อยู่นะจ๊ะ
- 2.2 config ไฟล์  WinSW.NET4.xml เพื่อตั้งค่าให้รัน jar file (ดูตัวอย่างในไฟล์เอานะ)
- 2.3 Run CMD by administrator
- 2.4 cd ไปยัง folder ที่ไฟล์ดังกล่าวจัดเก็บไว้อยู่
- 2.5 พิมพ์ WinSW.NET4.exe install
- 2.6 เข้าไปยัง local Services
- 2.7 ค้นหาชื่อ Service ตามที่ Config ใน WinSW.NET4.xml จากนั้นคลิกขวาแล้วสั่ง Start Services เป็นอัน จอบอ

>ref.https://www.youtube.com/watch?v=OOVE_g6F8mQ&t=359s
>ปล. ข้อดีการรันแบบนี้จะมี log file ให้ด้วย กับสามารถรัน Service บน local service กรณี windows startup ได้เลย
                
----

## ทั้งหมดทั้งมวลนี้ก็อย่าลืมติดตั้ง java กับ set env ลงเครื่องด้วยล่ะ Bye Bye